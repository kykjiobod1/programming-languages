import re

import requests

IGNORE_EXTENSIONS = ('jpeg', 'png', 'jpg', 'pdf', 'doc', 'docx',)
old_links = set()
all_emails = set()


def parse(url, domain=None, step=0, max_recursion=1):
    if step > max_recursion:
        return

    r_url = re.search('(?P<protocol>http[s]?)://(?P<domain>[^:/ ]+)', url)

    if domain is None:
        domain = r_url.group('domain')
    protocol = r_url.group('protocol')

    response = requests.get(url)
    links = re.findall(r'href=[\'"]?([^\'" >]+)', response.text)
    new_links = set()
    all_emails.update(re.findall(r'\b[\w.-]+?@\w+?\.\w+?\b', response.text))

    for link in links:
        if link[0] == '#' or link.split('.')[-1].lower() in IGNORE_EXTENSIONS:
            continue

        if link[:7] == 'mailto:':
            all_emails.add(link[7:])
            continue

        # Если нет протокола, то добавляю
        if link[:2] == '//':
            link = '{}:{}'.format(protocol, link)
        # Если нет домена, то добавляю
        elif link[0] == '/':
            link = '{}://{}{}'.format(protocol, domain, link)
        # Если путь относительный, то добавляю протокол и домен
        elif link[:7] != 'http://' and link[:8] != 'https://':
            link = '{}://{}/{}'.format(protocol, domain, link)

        r_url = re.search('(?P<protocol>http[s]?)://(?P<domain>[^:/ ]+)', link)

        # Перехожу по ссылкам только этого домена
        if domain != r_url.group('domain'):
            continue

        new_links.add(link)

    new_links = new_links - old_links
    old_links.update(new_links)
    print('.' * len(new_links))

    for link in new_links:
        parse(link, domain, step+1, max_recursion)


def main():
    print('Hello!')
    site_name = input('Please print site (e.g. "http://www.csd.tsu.ru/"):\n')
    parse(site_name)
    print('\n\nEmails:\n{}'.format('\n'.join(all_emails)))

    if input('Show links? (y/n): ') == 'y':
        print('\n'.join(old_links))


main()
