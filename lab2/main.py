import re


def main():
    ip_groups = {}

    f = open('access.log', 'r')

    for line in f:
        r = re.match(r'^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}', line)

        if r:
            ip = r.group()
        else:
            continue

        ip_group = '.'.join(ip.split('.')[:3])

        if ip_group in ip_groups:
            ip_groups[ip_group].add(ip)
        else:
            ip_groups[ip_group] = {ip}

    f.close()

    for ip_group in ip_groups.keys():
        print('\n'.join(ip_groups[ip_group]))
        print()


if __name__ == '__main__':
    main()
